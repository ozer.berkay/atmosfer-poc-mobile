import 'package:atmosfer/screens/authorization/login.dart';
import 'package:atmosfer/services/auth_service.dart';
import 'package:atmosfer/styles/custom_styles.dart';
import 'package:atmosfer/widgets/custom_theme_widgets.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nb_utils/nb_utils.dart';

class Signup extends StatefulWidget {
  const Signup({super.key});

  @override
  State<Signup> createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  final AuthService authService = AuthService();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _confirmPasswordController = TextEditingController();
  FocusNode emailFocusNode = FocusNode();
  FocusNode passWordFocusNode = FocusNode();
  FocusNode confirmPasswordFocusNode = FocusNode();
  final _formGlobalKey = GlobalKey<FormState>();

  String get _email => _emailController.text.trim();
  String get _password => _passwordController.text.trim();
  String get _confirmPassword => _confirmPasswordController.text.trim();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back_ios,
            size: 20,
            color: Colors.black,
          ),
        ),
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Form(
              key: _formGlobalKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      const SizedBox(
                        height: 50,
                      ),
                      const Text(
                        "Kayıt Ol",
                        style: TextStyle(
                            fontSize: 30, fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Yeni Bir Hesap Oluştur",
                        style: TextStyle(fontSize: 15, color: Colors.grey[700]),
                      ),
                      const SizedBox(
                        height: 50,
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30),
                    child: Column(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              "E-Posta",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black87),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            AppTextField(
                              decoration: CustomInputDecoration(
                                  bgColor: Colors.white,
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 0, horizontal: 10),
                                  focusedBorder: loginFocusedInputBorder,
                                  border: loginUnfocusedInputBorder,
                                  labelStyle: emailFocusNode.hasFocus
                                      ? loginFocusedLabelTextStyle
                                      : loginUnfocusedLabelTextStyle,
                                  prefixIcon: Icons.email_outlined),
                              textFieldType: TextFieldType.EMAIL,
                              keyboardType: TextInputType.emailAddress,
                              controller: _emailController,
                              validator: (mailStr) {
                                setState(() {});
                                if (mailStr == null || mailStr.isEmpty) {
                                  return "E-Posta alanı boş bırakılamaz";
                                } else {
                                  return EmailValidator.validate(_email)
                                      ? null
                                      : "Lütfen geçerli bir e-posta adresi giriniz";
                                }
                              },
                              onTap: () {
                                setState(() {});
                              },
                              focus: emailFocusNode,
                              nextFocus: passWordFocusNode,
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            const Text(
                              "Şifre",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black87),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            AppTextField(
                              decoration: CustomInputDecoration(
                                  bgColor: Colors.white,
                                  focusedBorder: loginFocusedInputBorder,
                                  border: loginUnfocusedInputBorder,
                                  labelStyle: passWordFocusNode.hasFocus
                                      ? loginFocusedLabelTextStyle
                                      : loginUnfocusedLabelTextStyle,
                                  prefixIcon: Icons.lock_outline),
                              suffixIconColor: Colors.grey,
                              textFieldType: TextFieldType.PASSWORD,
                              isPassword: true,
                              keyboardType: TextInputType.visiblePassword,
                              validator: (_) {
                                if (_confirmPassword != _password) {
                                  return "Girdiğiniz şifreler eşleşmemektedir!";
                                } else if (_password.isEmpty) {
                                  return "Şifre Alanı Boş Bırakılamaz!";
                                } else if (_email.contains("@") &&
                                    (_password.contains(_email) ||
                                        _password.contains(_email.substring(
                                            0, _email.indexOf("@"))))) {
                                  return "Şifreniz E-Posta adresinizi içeremez!";
                                } else if (_password.length < 8) {
                                  return "Şifreniz 8 karakterden küçük olamaz!";
                                } else if (!checkPassword(_password)) {
                                  return "Şifre 1 adet küçük-büyük harf ve sayı içermelidir!";
                                } else {
                                  return null;
                                }
                              },
                              onTap: () {
                                setState(() {});
                              },
                              controller: _passwordController,
                              focus: passWordFocusNode,
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            const Text(
                              "Şifreni Doğrula",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black87),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            AppTextField(
                              decoration: CustomInputDecoration(
                                  bgColor: Colors.white,
                                  focusedBorder: loginFocusedInputBorder,
                                  border: loginUnfocusedInputBorder,
                                  labelStyle: confirmPasswordFocusNode.hasFocus
                                      ? loginFocusedLabelTextStyle
                                      : loginUnfocusedLabelTextStyle,
                                  prefixIcon: Icons.lock_outline),
                              suffixIconColor: Colors.grey,
                              textFieldType: TextFieldType.PASSWORD,
                              isPassword: true,
                              keyboardType: TextInputType.visiblePassword,
                              validator: (_) {
                                if (_confirmPassword != _password) {
                                  return "Girdiğiniz şifreler eşleşmemektedir!";
                                } else if (_confirmPassword.isEmpty) {
                                  return "Şifre Doğrulama Alanı Boş Bırakılamaz!";
                                } else {
                                  return null;
                                }
                              },
                              onTap: () {
                                setState(() {});
                              },
                              controller: _confirmPasswordController,
                              focus: confirmPasswordFocusNode,
                            ),
                            const SizedBox(
                              height: 50,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30),
                    child: Container(
                      padding: const EdgeInsets.only(top: 3, left: 3),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          border: const Border(
                            bottom: BorderSide(color: Colors.black),
                            top: BorderSide(color: Colors.black),
                            left: BorderSide(color: Colors.black),
                            right: BorderSide(color: Colors.black),
                          )),
                      child: MaterialButton(
                        minWidth: double.infinity,
                        height: 50,
                        onPressed: () {
                          submitForm();
                        },
                        color: Colors.black,
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50)),
                        child: const Text(
                          "Kayıt Ol",
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      const SizedBox(
                        height: 50,
                      ),
                      const Text(
                        "Zaten bir hesabın var mı ? ",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                            color: Colors.black),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const Login()));
                        },
                        child: const Text(
                          "Giriş Yap",
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 20,
                              color: Colors.red),
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 100,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool checkPassword(String str) =>
      RegExp(r'[A-Z]').hasMatch(str) &&
          RegExp(r'[a-z]').hasMatch(str) &&
          RegExp(r'[0-9]').hasMatch(str);

  bool formStateNotValid() =>
      !(_formGlobalKey.currentState?.validate() ?? false);

  void submitForm() {
    if (formStateNotValid()) {
      return;
    }
    authService.register(_email, _password, context);
  }
}
