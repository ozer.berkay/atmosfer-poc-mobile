import 'package:atmosfer/screens/authorization/reset_password.dart';
import 'package:atmosfer/screens/authorization/signup.dart';
import 'package:atmosfer/screens/home/AtmosphereHome.dart';
import 'package:atmosfer/services/auth_service.dart';
import 'package:atmosfer/styles/custom_styles.dart';
import 'package:atmosfer/widgets/custom_theme_widgets.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nb_utils/nb_utils.dart';

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  AuthService authService = AuthService();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  FocusNode emailFocusNode = FocusNode();
  FocusNode passWordFocusNode = FocusNode();
  final _formGlobalKey = GlobalKey<FormState>();
  String get _email => _emailController.text.trim();
  String get _password => _passwordController.text.trim();
  bool loginResult = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: Column(
          children: <Widget>[
            Expanded(
              child: Form(
                key: _formGlobalKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        const Text(
                          "Giriş",
                          style: TextStyle(
                              fontSize: 30, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Hesabına Giriş Yap",
                          style: TextStyle(fontSize: 15, color: Colors.grey[700]),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      child: Column(
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "E-Posta",
                                style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.black87),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              AppTextField(
                                decoration: CustomInputDecoration(
                                  bgColor: Colors.white,
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 0, horizontal: 10),
                                  focusedBorder: loginFocusedInputBorder,
                                  border: loginUnfocusedInputBorder,
                                  labelStyle: emailFocusNode.hasFocus
                                      ? loginFocusedLabelTextStyle
                                      : loginUnfocusedLabelTextStyle,
                                    prefixIcon: Icons.email_outlined
                                ),
                                textFieldType: TextFieldType.EMAIL,
                                keyboardType: TextInputType.emailAddress,
                                controller: _emailController,
                                validator: (mailStr) {
                                  setState(() {});
                                  if (mailStr == null || mailStr.isEmpty) {
                                    return "E-Posta alanı boş bırakılamaz";
                                  } else if (!loginResult) {
                                    return "Kullanıcı adı veya şifre hatalı!";
                                  }else {
                                    return EmailValidator.validate(_email)
                                        ? null
                                        : "Lütfen geçerli bir e-posta adresi giriniz";
                                  }
                                },
                                onTap: () {
                                  setState(() {});
                                },
                                focus: emailFocusNode,
                                nextFocus: passWordFocusNode,
                              ),
                              const SizedBox(
                                height: 30,
                              ),
                              const Text(
                                "Şifre",
                                style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.black87),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              AppTextField(
                                decoration: CustomInputDecoration(
                                    bgColor: Colors.white,
                                    focusedBorder: loginFocusedInputBorder,
                                    border: loginUnfocusedInputBorder,
                                    labelStyle: passWordFocusNode.hasFocus
                                        ? loginFocusedLabelTextStyle
                                        : loginUnfocusedLabelTextStyle,
                                    prefixIcon: Icons.lock_outline),
                                suffixIconColor: Colors.grey,
                                textFieldType: TextFieldType.PASSWORD,
                                isPassword: true,
                                keyboardType: TextInputType.visiblePassword,
                                validator: (_) {
                                  if(_password.isEmpty) {
                                    return "Şifre boş bırakılamaz";
                                  } else if (!loginResult) {
                                    return "Kullanıcı adı veya şifre hatalı!";
                                  } else {
                                    return null ;
                                  }

                                },
                                onTap: () {
                                  setState(() {});
                                },
                                controller: _passwordController,
                                focus: passWordFocusNode,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      child: Container(
                        padding: const EdgeInsets.only(top: 3, left: 3),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            border: const Border(
                              bottom: BorderSide(color: Colors.black),
                              top: BorderSide(color: Colors.black),
                              left: BorderSide(color: Colors.black),
                              right: BorderSide(color: Colors.black),
                            )),
                        child: MaterialButton(
                          minWidth: double.infinity,
                          height: 50,
                          onPressed: () {
                            submitForm();
                          },
                          color: Colors.black,
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50)),
                          child: const Text(
                            "Giriş Yap",
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 18,
                                color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            const Text(
                              "Bir hesabın yok mu ? ",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 18),
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => const Signup()));
                              },
                              child: const Text(
                                "Kayıt Ol",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 20,
                                    color: Colors.red),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            const Text(
                              "Şifreni mi Unuttun ? ",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 18),
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => const ResetPassword()));
                              },
                              child: const Text(
                                "Şifremi Unuttum",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 20,
                                    color: Colors.red),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 100,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool formStateNotValid() =>
      !(_formGlobalKey.currentState?.validate() ?? false);

  Future<void> submitForm() async {
    loginResult = true;
    SharedPreferences _sharedPreferences = await SharedPreferences.getInstance();
    await _sharedPreferences.clear();
    if (formStateNotValid()) {
      return;
    }
    loginResult = await authService.login(_email, _password, context);
    setState(() {});
    loginResult == true ?
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => const AtmosphereHome())) : print("Kullanıcı adı veya şifre hatalı");
    // _formGlobalKey.currentState?.save();
    // bool isLoggedIn = await _authService.login(_email, _password, context);
    // if (!isLoggedIn) {
    //   isChecked = false;
    //   StagedLoginDB.deleteAllCredentials();
    //   refreshController.refreshCompleted();
    //   _stagedEmail = "";
    //   _stagedPassword = "";
    //   setState(() {});
    //   throw Exception('No authorized request');
    // }
    // if (isChecked) {
    //   StagedLoginDB.put(_email, _password);
    // }

  }
}
