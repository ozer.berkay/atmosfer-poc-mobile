import 'package:atmosfer/modal/Position.dart';
import 'package:atmosfer/services/position_service.dart';
import 'package:atmosfer/styles/app_colors.dart';
import 'package:atmosfer/widgets/home_widgets/position_card.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class AtmosphereHome extends StatefulWidget {
  const AtmosphereHome({super.key});

  @override
  State<AtmosphereHome> createState() => _AtmosphereHomeState();
}

class _AtmosphereHomeState extends State<AtmosphereHome> {
  PositionService positionService = PositionService();
  List<PositionModel> positions = [];
  final RefreshController refreshController =
      RefreshController(initialRefresh: true);
  final ScrollController scrollController = ScrollController();

  Future<bool> getPositions() async {
    try {
      var positionsResponse = await positionService.getAll();
      positions.addAll(positionsResponse);
      positions;
      return true;
    } catch (_) {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        shadowColor: Colors.grey,
        elevation: 5,
        leading: Transform.scale(
          scale: 1.1,
          child: Builder(
            builder: (context) {
              return Transform.translate(
                offset: const Offset(10, 0),
                child: IconButton(
                  splashRadius: 20,
                  icon: Image.asset(
                    "assets/images/turkcell_logo.png",
                    fit: BoxFit.cover,
                  ),
                  onPressed: () {
                    Scaffold.of(context).openDrawer();
                  },
                ),
              );
            },
          ),
        ),
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Turkcell Başvuru",
              textAlign: TextAlign.center,
              style: GoogleFonts.poppins(
                color: Colors.white,
                fontSize: 18,
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
        backgroundColor: Colors.black,
      ),
      body: SmartRefresher(
        controller: refreshController,
        onRefresh: () async {
          bool isPageRefreshed = await getPositions();
          if (isPageRefreshed) {
            refreshController.refreshCompleted();
            refreshController.loadComplete();
          } else {
            refreshController.refreshFailed();
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 25.0, vertical: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(top: 20, bottom: 20),
                  child: Text(
                    "Turkcell İş Başvurusu",
                    style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: homePageInlineTitleColor),
                  ),
                ),
                const Text(
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. sed do eiusmod tempor."),
                const Padding(
                  padding: EdgeInsets.only(top: 20, bottom: 20),
                  child: Text(
                    "Genel İş Tanımı",
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: homePageInlineTitleColor),
                  ),
                ),
                const Text(
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin at augue eget metus bibendum luctus. Sed eget sapien a augue pharetra blandit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas."),
                const Padding(
                  padding: EdgeInsets.only(top: 20, bottom: 20),
                  child: Text(
                    "Tercih Edilen Pozisyon Seçimi",
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: homePageInlineTitleColor),
                  ),
                ),
                ListView.builder(
                  shrinkWrap: true,
                  controller: scrollController,
                  itemCount: positions.length,
                  itemBuilder: (BuildContext context, int index) {
                    return positions.length > 0
                        ? PositionCard(
                            positionName: positions[index].name ?? "",
                            positionId: positions[index].id ?? "",
                      description: positions[index].detail ?? "",
                          )
                        : Text("");
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
