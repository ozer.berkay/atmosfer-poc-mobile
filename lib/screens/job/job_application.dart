import 'package:atmosfer/constants/cities_constant.dart';
import 'package:atmosfer/modal/position_application.dart';
import 'package:atmosfer/services/position_service.dart';
import 'package:atmosfer/styles/app_colors.dart';
import 'package:atmosfer/styles/custom_styles.dart';
import 'package:atmosfer/widgets/custom_theme_widgets.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:nb_utils/nb_utils.dart';

class JobApplication extends StatefulWidget {
  final String positionName;
  final String positionId;

  const JobApplication({Key? key, required this.positionName, required this.positionId}) : super(key: key);

  @override
  State<JobApplication> createState() => _JobApplicationState();
}

class _JobApplicationState extends State<JobApplication> {
  PositionService positionService = PositionService();
  final tcknController = TextEditingController();
  final nameController = TextEditingController();
  final surnameController = TextEditingController();
  final phoneController = TextEditingController();
  final cityController = TextEditingController();
  FocusNode tcknFocusNode = FocusNode();
  FocusNode nameFocusNode = FocusNode();
  FocusNode surnameFocusNode = FocusNode();
  FocusNode phoneFocusNode = FocusNode();
  FocusNode cityFocusNode = FocusNode();
  final _formGlobalKey = GlobalKey<FormState>();

  String get tckn => tcknController.text.trim();
  String get name => nameController.text.trim();
  String get surname => surnameController.text.trim();
  String get phone => phoneController.text.trim();
  String get city => cityController.text.trim();
  String? selectedCity;
  final sharedPreferences = SharedPreferences.getInstance();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        shadowColor: Colors.grey,
        elevation: 5,
        leading: Transform.scale(
          scale: 1.1,
          child: Builder(
            builder: (context) {
              return Transform.translate(
                offset: const Offset(10, 0),
                child: IconButton(
                  splashRadius: 20,
                  icon: Image.asset(
                    "assets/images/turkcell_logo.png",
                    fit: BoxFit.cover,
                  ),
                  onPressed: () {
                    Scaffold.of(context).openDrawer();
                  },
                ),
              );
            },
          ),
        ),
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Turkcell Başvuru",
              textAlign: TextAlign.center,
              style: GoogleFonts.poppins(
                color: Colors.white,
                fontSize: 18,
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
        backgroundColor: Colors.black,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(left: 30, top: 40),
              child: Row(
                children: [
                  GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: RichText(
                        text: const TextSpan(
                          children: [
                            WidgetSpan(
                              child: Icon(
                                Icons.arrow_back_ios,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                      )),
                  Text(
                    "Başvuran Bilgileri",
                    style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: homePageInlineTitleColor),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            Column(
              children: <Widget>[
                Form(
                  key: _formGlobalKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 30),
                        child: Column(
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "TC Kimlik Numarası (Zorunlu)",
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black87),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                AppTextField(
                                  decoration: CustomInputDecoration(
                                      bgColor: Colors.white,
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 0, horizontal: 10),
                                      focusedBorder: loginFocusedInputBorder,
                                      border: loginUnfocusedInputBorder,
                                      labelStyle: tcknFocusNode.hasFocus
                                          ? loginFocusedLabelTextStyle
                                          : loginUnfocusedLabelTextStyle,
                                      prefixIcon: Icons.email_outlined),
                                  textFieldType: TextFieldType.NUMBER,
                                  keyboardType: TextInputType.emailAddress,
                                  controller: tcknController,
                                  validator: (tckn) {
                                    setState(() {});
                                    if (tckn == null || tckn.isEmpty) {
                                      return "TC kimlik alanı boş bırakılamaz";
                                    }
                                  },
                                  onTap: () {
                                    setState(() {});
                                  },
                                  focus: tcknFocusNode,
                                  nextFocus: nameFocusNode,
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                const Text(
                                  "Ad (Zorunlu)",
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black87),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                AppTextField(
                                  decoration: CustomInputDecoration(
                                      bgColor: Colors.white,
                                      focusedBorder: loginFocusedInputBorder,
                                      border: loginUnfocusedInputBorder,
                                      labelStyle: nameFocusNode.hasFocus
                                          ? loginFocusedLabelTextStyle
                                          : loginUnfocusedLabelTextStyle,
                                      prefixIcon: Icons.lock_outline),
                                  suffixIconColor: Colors.grey,
                                  textFieldType: TextFieldType.NAME,
                                  isPassword: true,
                                  keyboardType: TextInputType.visiblePassword,
                                  validator: (_) {
                                    if (name.isEmpty) {
                                      return "Ad alanı boş bırakılamaz";
                                    }
                                  },
                                  onTap: () {
                                    setState(() {});
                                  },
                                  controller: nameController,
                                  focus: nameFocusNode,
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                const Text(
                                  "Soyad (Zorunlu)",
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black87),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                AppTextField(
                                  decoration: CustomInputDecoration(
                                      bgColor: Colors.white,
                                      focusedBorder: loginFocusedInputBorder,
                                      border: loginUnfocusedInputBorder,
                                      labelStyle: surnameFocusNode.hasFocus
                                          ? loginFocusedLabelTextStyle
                                          : loginUnfocusedLabelTextStyle,
                                      prefixIcon: Icons.lock_outline),
                                  suffixIconColor: Colors.grey,
                                  textFieldType: TextFieldType.NAME,
                                  isPassword: true,
                                  keyboardType: TextInputType.visiblePassword,
                                  validator: (_) {
                                    if (surname.isEmpty) {
                                      return "Soyad alanı boş bırakılamaz!";
                                    }
                                  },
                                  onTap: () {
                                    setState(() {});
                                  },
                                  controller: surnameController,
                                  focus: surnameFocusNode,
                                ),
                                const Text(
                                  "Şehir (Zorunlu)",
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black87),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                AppTextField(
                                  decoration: CustomInputDecoration(
                                      bgColor: Colors.white,
                                      focusedBorder: loginFocusedInputBorder,
                                      border: loginUnfocusedInputBorder,
                                      labelStyle: cityFocusNode.hasFocus
                                          ? loginFocusedLabelTextStyle
                                          : loginUnfocusedLabelTextStyle,
                                      prefixIcon: Icons.lock_outline),
                                  suffixIconColor: Colors.grey,
                                  textFieldType: TextFieldType.NAME,
                                  isPassword: true,
                                  keyboardType: TextInputType.visiblePassword,
                                  validator: (_) {
                                    if (city.isEmpty) {
                                      return "Şehir alanı boş bırakılamaz!";
                                    }
                                  },
                                  onTap: () {
                                    setState(() {});
                                  },
                                  controller: cityController,
                                  focus: cityFocusNode,
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                const Text(
                                  "Cep Telefonu (Zorunlu)",
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black87),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                AppTextField(
                                  decoration: CustomInputDecoration(
                                      bgColor: Colors.white,
                                      focusedBorder: loginFocusedInputBorder,
                                      border: loginUnfocusedInputBorder,
                                      labelStyle: phoneFocusNode.hasFocus
                                          ? loginFocusedLabelTextStyle
                                          : loginUnfocusedLabelTextStyle,
                                      prefixIcon: Icons.lock_outline),
                                  suffixIconColor: Colors.grey,
                                  textFieldType: TextFieldType.NAME,
                                  isPassword: true,
                                  keyboardType: TextInputType.visiblePassword,
                                  validator: (_) {
                                    if (phone.isEmpty) {
                                      return "Cep Telefonu alanı boş bırakılamaz!";
                                    }
                                  },
                                  onTap: () {
                                    setState(() {});
                                  },
                                  controller: phoneController,
                                  focus: phoneFocusNode,
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                const Text(
                                  "Şehir (Zorunlu)",
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black87),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Container(
                                  color: Colors.white,
                                  child: DropdownButton<String>(
                                    hint: Text("Şehir Seç"),
                                    value: selectedCity,
                                    items: cities.map((String city) {
                                      return DropdownMenuItem<String>(
                                        value: city,
                                        child: Text(city),
                                      );
                                    }).toList(),
                                    onChanged: (value) {
                                      setState(() {
                                        selectedCity = value;
                                      });
                                    },
                                  ),
                                ),
                                const SizedBox(
                                  height: 50,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      bottomNavigationBar: Material(
        color: Colors.black,
        child: InkWell(
          onTap: () {
            submitForm();
          },
          child: SizedBox(
            height: 70,
            width: double.infinity,
            child: Center(
              child: Text(
                "Başvur",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  color: Colors.white,
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> submitForm() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String email = prefs.get("email") as String;
    PositionApplication positionApplication = PositionApplication.all(city, email, name, phone, surname, tckn, widget.positionId);
    bool isConfirmed = false;
    bool applicationResult = false;
    await showDialog(context: context, builder: (BuildContext context) {
      return AlertDialog(
        title: Text(
            'Lütfen aydınlatma metnini okuyunuz!'),
        content: Text(
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        ),
        actions: <Widget>[
          ElevatedButton(
            child: Text('Kapat'),
            onPressed: () {
              isConfirmed = false;
              Navigator.of(context).pop();
            },
          ),
          ElevatedButton(
            child: Text('Okudum, Onaylıyorum'),
            onPressed: () async {
              isConfirmed = true;
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    }).then((value) async {
      if(isConfirmed) {
        applicationResult = await positionService.applyToPosition(positionApplication);
      }
    });


    await showDialog(context: context, builder: (BuildContext context) {
      return AlertDialog(
        title: Text(
            applicationResult ? "Başvurunuz Tamamlandı!" : "Başvuru tamamlanamadı!"),
        actions: <Widget>[
          ElevatedButton(
            child: Text('Kapat'),
            onPressed: () {
              isConfirmed = false;
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    }).then((value) async {
    });
  }
}
