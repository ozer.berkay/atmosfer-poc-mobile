import 'package:atmosfer/styles/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

InputBorder loginFocusedInputBorder = OutlineInputBorder(
  borderRadius: const BorderRadius.all(Radius.circular(16)),
  borderSide: BorderSide(color: Colors.black),
);

InputBorder loginUnfocusedInputBorder = OutlineInputBorder(
    borderRadius: const BorderRadius.all(
      Radius.circular(20.0),
    ),
    borderSide: BorderSide(color: Colors.grey[400]!));

InputBorder loginDefaultFocusedInputBorder = OutlineInputBorder(
  borderRadius: const BorderRadius.all(Radius.circular(16)),
  borderSide: BorderSide(color: Colors.grey.withOpacity(0.2)),
);

InputBorder loginDefaultUnfocusedInputBorder = OutlineInputBorder(
    borderRadius: const BorderRadius.all(
      Radius.circular(15.0),
    ),
    borderSide: BorderSide(
      color: Colors.grey[400]!,
    ));

InputBorder expertiseTabInputBorder = OutlineInputBorder(
  borderRadius: const BorderRadius.all(Radius.circular(16)),
  borderSide: BorderSide(color: Colors.white.withOpacity(0.2), width: 0),
);

TextStyle loginFocusedLabelTextStyle = GoogleFonts.poppins(
    fontWeight: FontWeight.w400, color: labelTextColor, fontSize: 22);

TextStyle loginUnfocusedLabelTextStyle = GoogleFonts.poppins(
    fontWeight: FontWeight.w400, color: labelTextColor, fontSize: 17);
