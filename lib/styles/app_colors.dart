import 'package:flutter/material.dart';

List<Color> webColors = [appCat1, appCat2, appCat3];

// Light Theme Colors
const appColorPrimary = Color(0xFF5685EC);
const appColorPrimaryDarker = Color(0XFF5959fc);
const appColorAccent = Color(0xFF03DAC5);
const appTextColorPrimary = Color(0xFF212121);
const iconColorPrimary = Color(0xFFFFFFFF);
const appTextColorSecondary = Color(0xFF5A5C5E);
const iconColorSecondary = Color(0xFFA8ABAD);
const appLayout_background = Color(0xFFf8f8f8);
const appLight_purple = Color(0xFFdee1ff);
const appLight_orange = Color(0xFFffddd5);
const appLight_parrot_green = Color(0xFFb4ef93);
const appIconTintCherry = Color(0xFFffddd5);
const appIconTint_sky_blue = Color(0xFF73d8d4);
const appIconTint_mustard_yellow = Color(0xFFffc980);
const appIconTintDark_purple = Color(0xFF8998ff);
const appTxtTintDark_purple = Color(0xFF515BBE);
const appIconTintDarkCherry = Color(0xFFf2866c);
const appIconTintDark_sky_blue = Color(0xFF73d8d4);
const appDark_parrot_green = Color(0xFF5BC136);
const appDarkRed = Color(0xFFF06263);
const appLightRed = Color(0xFFF89B9D);
const appCat1 = Color(0xFF8998FE);
const appCat2 = Color(0xFFFF9781);
const appCat3 = Color(0xFF73D7D3);
const appCat4 = Color(0xFF87CEFA);
const appCat5 = appColorPrimary;
const appShadowColor = Color(0x95E9EBF0);
const appColorPrimaryLight = Color(0xFFF9FAFF);
const appSecondaryBackgroundColor = Color(0xff343434);
const cardShadowColor = Color(0xFFC3C0BE);
const appDividerColor = Color(0xFFDADADA);
const appSplashSecondaryColor = Color(0xFFD7DBDD);
const inputLabelUnfocusedGreen = Color.fromRGBO(50, 212, 164, 0.5);
const inputLabelUnfocusedNavyBlue = Color.fromRGBO(49, 43, 79, 0.5);
const inputLabelUnfocusedGradient = [
  inputLabelUnfocusedGreen,
  inputLabelUnfocusedNavyBlue,
  inputLabelUnfocusedGreen
];
const inputLabelFocusedGreen = Color.fromRGBO(50, 212, 164, 1);
const inputLabelFocusedNavyBlue = Color.fromRGBO(49, 43, 79, 1);
const inputLabelFocusedGradient = [
  inputLabelFocusedGreen,
  inputLabelFocusedNavyBlue,
  inputLabelFocusedGreen
];
const homePageInlineTitleColor = Color.fromRGBO(67, 67, 67, 1);
const labelTextColor = Color.fromRGBO(136, 135, 144, 1);
const loginButtonBackground = Color.fromRGBO(50, 212, 164, 1);
const checkBoxGradient = [inputLabelFocusedGreen, inputLabelFocusedNavyBlue];
const menuAvatarBorderColor = Color.fromRGBO(50, 212, 164, 1);
const positionCardSplashColor = Color.fromRGBO(225, 255, 240, 1);
Color appBarShadowColor = Colors.white54;
const informationTabInfoColor = Color.fromRGBO(136, 135, 144, 1);
const informationTabInputTextColor = Color.fromRGBO(136, 135, 144, 1);
const expertiseTabInputBgColor = Color.fromRGBO(236, 251, 247, 1);
const expertiseTabSelectButtonsBgColor = Color.fromRGBO(50, 212, 164, 1);
Color mediaCardStagedBgColor = Colors.black.withOpacity(0.55);
Color mediaCardDbBgColor = Colors.white.withOpacity(0.65);
const Color mediaCardSelectedBgColor = Color.fromRGBO(50, 212, 164, 1);
const mediaCardSelectedBorderGradient = [
  Colors.green,
  Colors.lightGreenAccent,
  Colors.green
];
const mediaExpandedButtonBgColor = Color.fromRGBO(50, 212, 164, 1);

// Dark Theme Colors
const appBackgroundColorDark = Color(0xFF262626);
const cardBackgroundBlackDark = Color(0xFF1F1F1F);
const color_primary_black = Color(0xFF131d25);
const appColorPrimaryDarkLight = Color(0xFFF9FAFF);
const iconColorPrimaryDark = Color(0xFF212121);
const iconColorSecondaryDark = Color(0xFFA8ABAD);
const appShadowColorDark = Color(0x1A3E3942);
const appGreyColor = Color(0xFF808080);
const CustomPrimaryColor = Color(0xFF6C56F9);
const CustomAccentColor = Color(0xFF26C884);

class AppColors {
  static const lightsky = Color(0xFFA6C0FF);
  static const whiteshade = Color(0xFFF8F9FA);
  static const blue = Color(0xFF497fff);
  static const lightblueshade = Color(0xFF758CC8);
  static const grayshade = Color(0xFFEBEBEB);
  static const lightblue = Color(0xFF4B68D1);
  static const blackshade = Color(0xFF555555);
  static const hintText = Color(0xFFC7C7CD);
}
