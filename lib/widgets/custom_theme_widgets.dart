import 'package:atmosfer/styles/app_colors.dart';
import 'package:atmosfer/styles/custom_styles.dart';
import 'package:flutter/material.dart';
import 'package:nb_utils/nb_utils.dart';

InputDecoration CustomInputDecoration(
    {IconData? prefixIcon,
    String? prefixText,
    String? hint,
    FloatingLabelBehavior? floatingLabelBehavior,
    Color? bgColor,
    TextStyle? labelStyle,
    InputBorder? focusedBorder,
    InputBorder? border,
    EdgeInsets? padding,
    String? labelText}) {
  return InputDecoration(
    contentPadding:
        padding ?? const EdgeInsets.symmetric(vertical: 16, horizontal: 16),
    counter: const Offstage(),
    focusedBorder: focusedBorder ?? loginDefaultFocusedInputBorder,
    enabledBorder: border ?? loginDefaultUnfocusedInputBorder,
    focusedErrorBorder: focusedBorder ?? loginDefaultFocusedInputBorder,
    errorBorder: border ?? loginDefaultUnfocusedInputBorder,
    prefixText: prefixText,
    labelText: labelText,
    labelStyle: labelStyle,
    floatingLabelBehavior: floatingLabelBehavior,
    fillColor: bgColor ?? CustomPrimaryColor.withOpacity(0.04),
    hintText: hint,
    prefixIcon: prefixIcon != null
        ? Icon(prefixIcon, color: mediaCardStagedBgColor)
        : null,
    hintStyle: secondaryTextStyle(),
    filled: true,
  );
}