import 'dart:io' as io;

import 'package:atmosfer/config/environment.dart';
import 'package:atmosfer/constants/http_constant.dart';
import 'package:atmosfer/constants/shared_preferences_constants.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BaseService {
  final sharedPreferences = SharedPreferences.getInstance();

  Dio createDio({String? accept}) {
    Dio dio = Dio();
    dio.options.baseUrl = Environment().config.apiHost;
    dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (options, handler) async {
          options.headers[HttpConstant.contentType] =
              HttpConstant.applicationJson;
          options.headers[HttpConstant.accept] = accept ?? HttpConstant.applicationJson;
          SharedPreferences prefs = await SharedPreferences.getInstance();
          options.headers[HttpConstant.authorization] =
              prefs.get(SharedPreferencesConstants.token);

          return handler.next(options);
        },
        onResponse: (response, handler) {
          if (response.statusCode == io.HttpStatus.unauthorized) {
            throw DioError(
                requestOptions: response.requestOptions,
                response: response,
                type: DioErrorType.badResponse,
                error: "Http status error [401]");
          }
          return handler.next(response);
        },
        onError: (DioError e, handler) {
          return handler.next(e);
        },
      ),
    );

    return dio;
  }
}
