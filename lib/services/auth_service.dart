import 'dart:convert';
import 'dart:io';

import 'package:atmosfer/constants/http_constant.dart';
import 'package:atmosfer/services/base_service.dart';
import 'package:atmosfer/services/shared_service.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class AuthService extends BaseService {
  final prefsService = SharedService();

  register(String email, String password, BuildContext context) async {
    const url = HttpConstant.register;
    final Response response;
    TextEditingController confirmNumberController = TextEditingController();
    bool isRegisterCompleted = false;

    try {
      response = await createDio().post(url,
          data: jsonEncode(
              <String, String>{'email': email, 'password': password}));
      if (response.statusCode == HttpStatus.ok ||
          response.statusCode == HttpStatus.created) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                  'Lütfen mail adresinize gelen doğrulama kodunu giriniz.'),
              content: TextField(
                controller: confirmNumberController,
                decoration: InputDecoration(hintText: 'Doğrulama Kodu'),
              ),
              actions: <Widget>[
                isRegisterCompleted ? Text(
                  "Hatalı Bir Kod Girdiniz!",
                  style: TextStyle(color: Colors.red),
                ): SizedBox(height: 0,),
                ElevatedButton(
                  child: Text('Kapat'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                ElevatedButton(
                  child: Text('Devam Et'),
                  onPressed: () async {
                    isRegisterCompleted = await completeRegister(
                        email, confirmNumberController.text, context);
                    if(isRegisterCompleted) {
                      Navigator.of(context).pop();
                    }
                  },
                ),
              ],
            );
          },
        );
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Hesabınız Başarıyla Oluşturuldu'),
              actions: <Widget>[
                ElevatedButton(
                  child: const Text('Kapat'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
        return true;
      } else {
        return false;
      }
    } on DioError catch (e) {
      return false;
    }
  }

  completeRegister(
      String email, String? verificationCode, BuildContext context) async {
    String url = HttpConstant.completeRegister;
    url = url.replaceFirst("{verificationCode}", verificationCode ?? "");
    url = url.replaceFirst("{email}", email);

    final Response response;

    try {
      response = await createDio().put(
        url,
      );
      if (response.statusCode == HttpStatus.ok ||
          response.statusCode == HttpStatus.created) {
        return true;
      } else {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Yanlış Bir Kod Girdiniz', style: TextStyle(color: Colors.red),),
              actions: <Widget>[
                ElevatedButton(
                  child: const Text('Kapat'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
        return false;
      }
    } on DioError catch (e) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Yanlış Bir Kod Girdiniz', style: TextStyle(color: Colors.red),),
            actions: <Widget>[
              ElevatedButton(
                child: const Text('Kapat'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
      return false;
    }
  }

  Future<bool>login(String email, String password, BuildContext context) async {
    const url = HttpConstant.loginFirstStep;
    final Response response;
    TextEditingController confirmNumberController = TextEditingController();
    FocusNode confirmNumberFocusNode = FocusNode();

    bool isLoginCompleted = false;

    try {
      response = await createDio().post(url,
          data: jsonEncode(
              <String, String>{'email': email, 'password': password}));
      if (response.statusCode == HttpStatus.ok ||
          response.statusCode == HttpStatus.created) {
        await showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text(
                  'Lütfen mail adresinize gelen doğrulama kodunu giriniz'),
              content: TextField(
                controller: confirmNumberController,
                decoration: InputDecoration(hintText: 'Doğrulama Kodu'),
              ),
              actions: <Widget>[
                ElevatedButton(
                  child: Text('Kapat'),
                  onPressed: () {
                    Navigator.of(context).pop();                  },
                ),
                ElevatedButton(
                  child: Text('Devam Et'),
                  onPressed: () async {
                    isLoginCompleted = await completeLogin(
                        email,password, confirmNumberController.text, context);
                    if(isLoginCompleted) {
                      Navigator.of(context).pop();                    }
                  },
                ),
              ],
            );
          },
        ).then((value) => true);
        return true;
      } else {
        return false;
      }
    } on DioError catch (e) {
      return false;
    }
  }

  completeLogin(
      String email, String password, String? verificationCode, BuildContext context) async {
    String url = HttpConstant.completeLogin;

    final Response response;

    try {
      response = await createDio(accept: "text/plain").put(
        url, data: jsonEncode(
          <String, String>{'otp': verificationCode ?? "", 'username': email, 'password': password}));
      print(response);
      if (response.statusCode == HttpStatus.ok ||
          response.statusCode == HttpStatus.created) {
        prefsService.updateSharedPreferences(response.data, email);
        return true;
      } else {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Yanlış Bir Kod Girdiniz', style: TextStyle(color: Colors.red),),
              actions: <Widget>[
                ElevatedButton(
                  child: const Text('Kapat'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
        return false;
      }
    } on DioError catch (e) {
      print(e);
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Yanlış Bir Kod Girdiniz', style: TextStyle(color: Colors.red),),
            actions: <Widget>[
              ElevatedButton(
                child: const Text('Kapat'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
      return false;
    }
  }
}
