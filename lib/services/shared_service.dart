import 'package:shared_preferences/shared_preferences.dart';

class SharedService {
  void updateSharedPreferences(String token, String email) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setString("token", "Bearer $token");
    prefs.setString("email", "Bearer $email");
  }
}
