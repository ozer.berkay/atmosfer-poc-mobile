import 'dart:io';

import 'package:atmosfer/constants/http_constant.dart';
import 'package:atmosfer/modal/Position.dart';
import 'package:atmosfer/modal/position_application.dart';
import 'package:atmosfer/services/base_service.dart';
import 'package:dio/dio.dart';

class PositionService extends BaseService {
  Future<List<PositionModel>> getAll() async {
    var url = HttpConstant.allPositions;
    Response resp = await createDio().get(url);

    return ((resp.data) as List)
        .map((message) => PositionModel.fromJson(message))
        .toList();
  }

  Future<bool> applyToPosition(PositionApplication position) async {
    var url = HttpConstant.applyToPosition;
    url = url.replaceFirst("{id}", position.positionId!);
    try {
      Response response = await createDio().post(url, data: position);
      if (response.statusCode == HttpStatus.ok ||
          response.statusCode == HttpStatus.created) {
        return true;
      }
      return false;
    } catch (_) {
      return false;
    }
  }
}
