import 'package:atmosfer/config/base_config.dart';
import 'package:atmosfer/config/dev_config.dart';
import 'package:atmosfer/config/test_config.dart';
import 'package:atmosfer/config/uat_config.dart';

class Environment {
  factory Environment() {
    return _singleton;
  }

  Environment._internal();

  static final Environment _singleton = Environment._internal();

  static const String DEV = 'DEV';
  static const String TEST = 'TEST';
  static const String UAT = 'UAT';

  late BaseConfig config;

  initConfig(String environment) {
    config = _getConfig(environment);
  }

  BaseConfig _getConfig(String environment) {
    switch (environment) {
      case Environment.UAT:
        return UATConfig();
      case Environment.TEST:
        return TestConfig();
      default:
        return DevConfig();
    }
  }
}