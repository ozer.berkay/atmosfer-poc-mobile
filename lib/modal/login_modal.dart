class LoginModal {
  String? email;
  String? password;
  num? otp;

  LoginModal.all(this.email,
      this.password, this.otp);

  Map<String, dynamic> toJson() => {
    'email': email,
    'password': password,
    'otp': otp,
  };

  factory LoginModal.fromJson(Map<String, dynamic> json) {
    return LoginModal.all(
      json['email'],
      json['password'],
      json['otp'],
    );
  }
}