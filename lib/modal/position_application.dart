class PositionApplication {
  String? city;
  String? email;
  String? name;
  String? phone;
  String? surname;
  String? tckn;
  String? positionId;

  PositionApplication.all(this.city,
      this.email, this.name, this.phone, this.surname, this.tckn, this.positionId);

  Map<String, dynamic> toJson() => {
    'city': city,
    'email': email,
    'name': name,
    'phone': phone,
    'surname': surname,
    'tckn': tckn,
    'positionId': positionId,
  };

  factory PositionApplication.fromJson(Map<String, dynamic> json) {
    return PositionApplication.all(
      json['city'],
      json['email'],
      json['name'],
      json['phone'],
      json['surname'],
      json['tckn'],
      json['positionId'],
    );
  }
}