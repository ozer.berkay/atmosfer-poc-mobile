class RegisterModel {
  String? email;
  String? password;
  num? otp;

  RegisterModel.all(this.email, this.password, this.otp);

  Map<String, dynamic> toJson() => {
    'email': email,
    'password': password,
    'otp': otp,
  };

  factory RegisterModel.fromJson(Map<String, dynamic> json) {
    return RegisterModel.all(
      json['email'],
      json['password'],
      json['otp'],
    );
  }
}