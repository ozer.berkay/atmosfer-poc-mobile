class PositionModel {
  String? id;
  String? name;
  String? detail;
  String? city;
  int? applicantCount;

  PositionModel.all(this.id,
      this.name, this.detail, this.city, this.applicantCount);

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'detail': detail,
    'city': city,
    'applicantCount': applicantCount,
  };

  factory PositionModel.fromJson(Map<String, dynamic> json) {
    return PositionModel.all(
      json['id'],
      json['name'],
      json['detail'],
      json['city'],
      json['applicantCount'],
    );
  }
}