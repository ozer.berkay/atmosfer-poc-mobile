class SharedPreferencesConstants {
  static const String jti = "jti";
  static const String userId = "user_id";
  static const String companyLogo = "company_logo";
  static const String companyName = "company_name";
  static const String userName = "name";
  static const String teamName = "team_name";
  static const String token = "token";
  static const String userMail = "sub";
  static const String expirationTime = "exp";
}
