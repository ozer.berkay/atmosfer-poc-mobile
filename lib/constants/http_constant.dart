class HttpConstant {
  static const String register = ":9001/auth/app-user/register-first-step";
  static const String completeRegister = ":9001/auth/app-user/register-verification/mail/{email}/code/{verificationCode}";
  static const String loginFirstStep = ":9001/auth/login-first-step";
  static const String completeLogin = ":9001/auth/login-verification";
  static const String allPositions = ":9000/position/all-open-positions";
  static const String applyToPosition = ":9000/position/id/{id}/apply";
  static const String authorization = "Authorization";
  static const String contentType = "Content-Type";
  static const String accept = "Accept";
  static const String applicationJson = "application/json";
}
